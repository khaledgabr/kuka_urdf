# CMake generated Testfile for 
# Source directory: /home/egyrobo/cc_ws/src
# Build directory: /home/egyrobo/cc_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(sh_urdf)
subdirs(sh_gazebo)
