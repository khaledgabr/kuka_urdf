set(_CATKIN_CURRENT_PACKAGE "sh_gazebo")
set(sh_gazebo_VERSION "0.0.0")
set(sh_gazebo_MAINTAINER "egyrobo <egyrobo@todo.todo>")
set(sh_gazebo_PACKAGE_FORMAT "2")
set(sh_gazebo_BUILD_DEPENDS "gazebo" "geometry_msgs" "roscpp" "rospy" "std_msgs" "tf" "urdf" "xacro")
set(sh_gazebo_BUILD_EXPORT_DEPENDS "gazebo" "geometry_msgs" "roscpp" "rospy" "std_msgs" "tf" "urdf" "xacro")
set(sh_gazebo_BUILDTOOL_DEPENDS "catkin")
set(sh_gazebo_BUILDTOOL_EXPORT_DEPENDS )
set(sh_gazebo_EXEC_DEPENDS "gazebo" "geometry_msgs" "roscpp" "rospy" "std_msgs" "tf" "urdf" "xacro")
set(sh_gazebo_RUN_DEPENDS "gazebo" "geometry_msgs" "roscpp" "rospy" "std_msgs" "tf" "urdf" "xacro")
set(sh_gazebo_TEST_DEPENDS )
set(sh_gazebo_DOC_DEPENDS )
set(sh_gazebo_URL_WEBSITE "")
set(sh_gazebo_URL_BUGTRACKER "")
set(sh_gazebo_URL_REPOSITORY "")
set(sh_gazebo_DEPRECATED "")